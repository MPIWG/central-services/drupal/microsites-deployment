# Quick install on a new server

```bash
git clone git@gitlab.gwdg.de:MPIWG/microsites-deployment.git drupal
cd drupal
mkdir files
cp example.env .env
```

Edit `.env` file to set a new virtual host.
```yml
VIRTUAL_HOST=my.host.com
VIRTUAL_GROUP=mygroup
```

Set a a docker tag
```yml
VERSION=stage
```

All variables declared in .env file, could be use as environment variable in docker files.

Multiple domains
```yml
VIRTUAL_HOST=my.host.com,other.host.com
```

## Install docker

Use the init script:
```bash
./ctrl init
```

Or
- install latest docker
- install latest docker-compose (using pip!)
- install all requirements
- add user to group docker

## Log in to docker repository
```bash
docker login docker.gitlab.gwdg.de
```
(use Deploy Token created in https://gitlab.gwdg.de/MPIWG/mpiwg-microsite/settings/repository)

## Start containers
```sh
docker-compose up -d
```

## Install and configure Drupal

Run install script for the desired *profile* e.g. `mpiwg`
```sh
./ctrl install mpiwg
```

# Update to latest image
```sh
docker-compose pull
docker-compose up -d
```

After an update in the Drupal version you should also run a database update:
```
./ctrl drush updb
```

# Add custom configuration

```bash
cp docker-compose.override.example.yml docker-compose.override.yml
```

Now you can override every service declared in standard `docker-compose.yml`

# Add predefined extra service

All extra services like redis or memcache should be globally defined in `docker-compose.extends.yml`.
These services can then be used by `docker-compose.override.yml`

Add in docker-compose.override.yml 
```yml
version: "2.4"

services:
  redis:
    extends:
      file: ./docker-compose.extends.yml
      service: redis

```

# CTRL Commands

You can add commands in the scripts folder. The name of the file is the parameter for ctrl.

For example
```sh
./ctrl install minimal
```
calls the script `scripts/install.sh`

## Existing commands

### Install

```sh
./ctrl install <profile-to-install>
```

### Status
```sh
./ctrl status
```

### Update
```sh
./ctrl update
```

### Drush
```sh
./ctrl drush <command>
```

### Call drush with docker-compose
```sh
docker-compose exec -w /var/www/docroot drupal drush <command>
```

# Customizations

## Add a static site directory

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  drupal:
    volumes:
      - ${PWD}/static-files:/var/www/docroot/static
```


## Add an Apache virtual host directive

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  drupal:
    volumes:
      - ${PWD}/configs/apache/my-vhost.conf:/opt/docker/etc/httpd/vhost.common.d/my-vhost.conf
```

## Add general Apache configuration

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  drupal:
    volumes:
      - ${PWD}/configs/apache/my.conf:/opt/docker/etc/httpd/conf.d/10-my.conf
```

## Add backup service

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  backup:
    extends:
      file: ./docker-compose.extends.yml
      service: backup
    environment:
       - CRON_TIME=0 0 * * *
```

## Add PHP configuration

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  drupal:
    volumes:
      - ${PWD}/configs/php/php.ini:/opt/docker/etc/php/php.ini
```

## Add MYSQL configuration

In `docker-compose.override.yml`
```yml
version: "2.4"

services:
  db:
    volumes:
      - ${PWD}/configs/mysql/drupal.cnf:/etc/mysql/conf.d/drupal.cnf
```

## Add SSL/TLS certificate

Outside connections are teminated by jwilder/nginx-proxy container. Check its [documentation](https://github.com/nginx-proxy/nginx-proxy) for details.

Copy private key to a file named after full host name with `key` extension to `certs` directory e.g. `certs/zwg.mpiwg-berlin.mpg.de.key`.

Copy full certificate (including certificate chain) to similar file with `crt` extension e.g. `certs/zwg.mpiwg-berlin.mpg.de.crt`.

## Add redirects for other hostnames

Let the container listen for all host names by adding all names to the `VIRTUAL_HOST` env variable (see above).

Expose Nginx vhost.d in `docker-compose.override.yml`
```yml
version: "2.4"

volumes:
  nginx-vhost:
    driver: local
    driver_opts:
      type: none
      device: "${PWD}/configs/nginx-vhost"
      o: bind
```

Add config files for each host name that you want to redirect to `configs/nginx-vhost` e.g. `configs/nginx-vhost/wissensgeschichte-berlin.de` containing e.g.
```
rewrite ^/(.*)$ https://zwg.mpiwg-berlin.mpg.de/$1 redirect;
```

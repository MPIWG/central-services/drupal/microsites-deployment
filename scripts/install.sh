#!/bin/sh

docker-compose exec -T -w /var/www/docroot drupal /bin/bash -c "drush si $1 --db-url=mysql://drupal:drupal@db/drupal -y"
#!/bin/sh

if [ ! -f .env ]
then
    echo "Please specify VIRTUAL_HOST"
    read VIRTUAL_HOST
    echo "Please specify VIRTUAL_GROUP"
    read VIRTUAL_GROUP

    echo "VIRTUAL_HOST=${VIRTUAL_HOST}" >> .env
    echo "VIRTUAL_GROUP=${VIRTUAL_GROUP}" >> .env
else
    echo "nok"
fi

echo "Update apt sources ..."
sudo apt update -qq
echo "Install needed packages ..."
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y -qqsfsfsdf
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "Install docker ..."
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" -n
sudo apt update -qq
sudo apt install docker-ce -y -qq

echo "Add user ${USER} to docker group"
sudo usermod -aG docker ${USER}

sudo apt install python-pip
source ~/.profile

echo "Add install docker-compose"
sudo pip install docker-compose

echo "Create empty files and certs directory"
mkdir -p ./files
mkdir -p ./certs

echo "Load group"
newgrp docker

#!/bin/sh

docker-compose exec -T -w /var/www/docroot drupal /bin/bash -c "drush $1 $2 $3 $4 $5 $6 $7 $8 $9"
